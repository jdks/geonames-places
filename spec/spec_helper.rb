$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'geonames/places'

RSpec.configure do |config|
  config.disable_monkey_patching!
  config.order = :random
end

module Geonames
  module Places
    RSpec.describe Configuration do
      let(:configuration) { Configuration.instance }

      describe '#domain' do
        subject { configuration.domain }

        let(:domain) { 'https://private.api.geonames.org' }
        let(:default_domain) { 'http://api.geonames.org' }

        it { is_expected.to eql(default_domain) }

        context 'domain configured' do
          around do |example|
            old_domain = configuration.domain
            configuration.domain = domain
            example.run
            configuration.domain = old_domain
          end

          it { is_expected.to eql(domain) }
        end
      end

      describe '#username' do
        subject { configuration.username }

        let(:username) { 'sally' }

        around do |example|
          old_username = configuration.username
          configuration.username = username
          example.run
          configuration.username = old_username
        end

        it { is_expected.to eql(username) }
      end

      describe '#token' do
        subject { configuration.token }

        let(:token) { '726f83e3' }

        around do |example|
          old_token = configuration.token
          configuration.token = token
          example.run
          configuration.token = old_token
        end

        it { is_expected.to eql(token) }
      end

      describe '#auth_params' do
        subject { configuration.auth_params }

        let(:username) { 'joe' }
        let(:token) { nil }

        around do |example|
          old_token = configuration.token
          old_username = configuration.username
          configuration.token = token if token
          configuration.username = username
          example.run
          configuration.token = old_token if token
          configuration.username = old_username
        end

        it { is_expected.to eql(username: username) }

        context 'token provided' do
          let(:token) { '726f83e3' }

          let(:auth_params) {
            {
              username: username,
              token: token
            }
          }

          it { is_expected.to eql(auth_params) }
        end
      end
    end
  end
end

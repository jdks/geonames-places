module Geonames
  module Places
    RSpec.describe API do
      let(:includer) { Class.include(API).new }

      let(:options) { { geoname_id: 3175395 } }

      after { subject }

      describe '#children' do
        subject { includer.children(options) }

        it 'creates a new request' do
          expect(Request).to receive(:new).with(:children, params: options)
        end
      end

      describe '#siblings' do
        subject { includer.siblings(options) }

        it 'creates a new request' do
          expect(Request).to receive(:new).with(:siblings, params: options)
        end
      end

      describe '#hierarchy' do
        subject { includer.hierarchy(options) }

        it 'creates a new request' do
          expect(Request).to receive(:new).with(:hierarchy, params: options)
        end
      end

      describe '#contains' do
        subject { includer.contains(options) }

        it 'creates a new request' do
          expect(Request).to receive(:new).with(:contains, params: options)
        end
      end

      describe '#neighbours' do
        subject { includer.neighbours(options) }

        it 'creates a new request' do
          expect(Request).to receive(:new).with(:neighbours, params: options)
        end
      end
    end
  end
end

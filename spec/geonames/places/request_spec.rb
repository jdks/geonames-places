module Geonames
  module Places
    RSpec.describe Request do
      let(:request) { Request.new(endpoint, params: options) }

      let(:endpoint) { :childrenJSON }
      let(:options) { { geoname_id: 3175395 } }

      describe '#response' do
        subject { request.response }

        let(:response) { double('HTTP::Response') }
        let(:configuration) {
          double(
            'Configuration',
            domain: domain,
            auth_params: auth_params
          )
        }
        let(:auth_params) { { username: 'joe' } }
        let(:domain) { 'http://api.geonames.org' }
        let(:params) { double('Request::Params', to_hash: normalized_params) }
        let(:client) { double('HTTP::Client') }
        let(:uri) { double('URI::HTTP') }
        let(:normalized_params) { {} }

        before do
          allow(Configuration).to receive(:instance) { configuration }
          allow(Request::Params).to receive(:new).
            with(options.merge(auth_params).merge(type: :json)) { params }
          allow(HTTP).to receive(:headers).
            with(accept: 'application/json') { client }
          allow(client).to receive(:get).
            with(uri, params: normalized_params) { response }
          allow(URI).to receive(:join).with(domain, endpoint.to_s) { uri }
        end

        it { is_expected.to eql(response) }
      end
    end
  end
end

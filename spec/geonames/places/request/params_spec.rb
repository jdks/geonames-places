module Geonames
  module Places
    class Request
      RSpec.describe Params do
        let(:params) { Params.new(opts) }

        let(:opts) {
          {
            domain: 'http://api.geonames.org',
            username: 'jackie',
            geoname_id: 72728383,
          }
        }

        describe '#to_hash' do
          subject { params.to_hash }

          let(:normalized_params) {
            {
              domain: 'http://api.geonames.org',
              username: 'jackie',
              geonameId: 72728383,
            }
          }

          it { is_expected.to eql(normalized_params) }
        end
      end
    end
  end
end

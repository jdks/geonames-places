RSpec.describe Geonames::Places do
  it 'has a version number' do
    expect(Geonames::Places::VERSION).to match(/(\d+\.){2}\d+/)
  end

  describe '.configure' do
    let(:configuration) { double('Geonames::Places::Configuration') }

    before do
      allow(Geonames::Places::Configuration).
        to receive(:instance) { configuration }
    end

    it 'yields the configuration block' do
      expect { |b| Geonames::Places.configure(&b) }.
        to yield_with_args(configuration)
    end
  end
end

require 'geonames/places/version'
require 'geonames/places/configuration'
require 'geonames/places/record'
require 'geonames/places/request'
require 'geonames/places/api'

module Geonames
  module Places
    extend API

    def self.configure
      yield(Configuration.instance) if block_given?
    end
  end
end

module Geonames
  module Places
    class Request
      class Params
        extend Forwardable

        def_delegators :@params, :keys, :values

        def initialize(params)
          @params = params
        end

        def to_hash
          normalized_keys.zip(values).to_h
        end

        private

        def normalize(key)
          key.gsub(/_(\p{Ll})/) { |match| match[/\p{Ll}/].upcase }
        end

        def normalized_keys
          method_chain.inject(keys, &map_proc)
        end

        def map_proc
          -> (acc, m) { acc.map(&m) }
        end

        def method_chain
          [
            :to_s,
            (method :normalize),
            :to_sym,
          ]
        end
      end
    end
  end
end

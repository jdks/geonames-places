module Geonames
  module Places
    module API
      def children(options)
        Request.new(:children, params: options)
      end

      def siblings(options)
        Request.new(:siblings, params: options)
      end

      def hierarchy(options)
        Request.new(:hierarchy, params: options)
      end

      def contains(options)
        Request.new(:contains, params: options)
      end

      def neighbours(options)
        Request.new(:neighbours, params: options)
      end
    end
  end
end

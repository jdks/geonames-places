require 'singleton'

module Geonames
  module Places
    class Configuration
      include Singleton

      attr_accessor :username, :token
      attr_writer :domain

      def domain
        @domain || default_domain
      end

      def auth_params
        {
          username: username,
          token: token,
        }.reject { |_, value| value.nil? }
      end

      private
      
      def default_domain
        'http://api.geonames.org'
      end
    end
  end
end

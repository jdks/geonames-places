require 'http'
require 'uri'
require 'forwardable'

require 'geonames/places/request/params'

module Geonames
  module Places
    class Request
      extend Forwardable

      def_delegators :configuration, :domain, :auth_params

      attr_reader :endpoint

      def initialize(endpoint, params:)
        @endpoint, @params = endpoint, params
      end

      def response
        client.get(uri, params: params.to_hash)
      end

      private

      def client
        HTTP.headers(headers)
      end

      def headers
        { accept: 'application/json' }
      end

      def uri
        URI.join(domain, endpoint.to_s)
      end

      def params
        Params.new(
          @params.merge(auth_params).merge(type: :json)
        )
      end
      
      def configuration
        Configuration.instance
      end
    end
  end
end
